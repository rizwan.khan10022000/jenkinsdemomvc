package com.telusko.service;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class AllServiceTest {
	
		AllService a = new AllService();
	@Test
	void testAdd() {
		int expected = 9;
		int actual = a.add(3,6);
		assertEquals(expected, actual);
	}

	@Test
	void testSubtract() {
		int expected = 8;
		int actual = a.subtract(10,2);
		assertEquals(expected, actual);
	}

	@Test
	void testMultiply() {
		int expected = 25;
		int actual = a.multiply(5,5);
		assertEquals(expected, actual);
	}

	@Test
	void testDevide() {
		int expected = 5;
		int actual = a.devide(50,10);
		assertEquals(expected, actual);
	}

}
