package com.telusko;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.telusko.service.AllService;

@Controller
public class AllController 
{
	
	@RequestMapping("answer")
	public ModelAndView add(HttpServletRequest request, HttpServletResponse response)
	{
		int i = Integer.parseInt(request.getParameter("t1"));
		int j = Integer.parseInt(request.getParameter("t2"));
		int k=0;
		
		AllService as=new AllService();
		
		
		if(request.getParameter("submitbutton").equals("+"))
		{
			k=as.add(i, j);
		}
		else if(request.getParameter("submitbutton").equals("-"))
		{
			k=as.subtract(i, j);
		}
		else if(request.getParameter("submitbutton").equals("*"))
		{
			k=as.multiply(i, j);
		}
		else if(request.getParameter("submitbutton").equals("/"))
		{
			k=as.devide(i, j);
		}
		
		ModelAndView mv= new ModelAndView();
		mv.setViewName("display.jsp");
		mv.addObject("result",k);
		
		return mv;
	}

}
